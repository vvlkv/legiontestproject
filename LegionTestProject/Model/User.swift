//
//  User.swift
//  LegionTestProject
//
//  Created by Виктор on 25/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

typealias Point = (x: Int, y: Int)

struct User {
    let id: Int
    let firstName: String
    let lastName: String
    let avatarUrl: String
    var position: Point
}
