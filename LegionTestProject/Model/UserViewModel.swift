//
//  UserViewModel.swift
//  LegionTestProject
//
//  Created by Виктор on 28/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import UIKit

struct UserViewModel {
    let id: Int
    let fullName: String
    let avatarUrl: String
    var distance: Int
}
