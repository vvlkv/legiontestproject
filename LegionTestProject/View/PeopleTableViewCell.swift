//
//  PeopleTableViewCell.swift
//  LegionTestProject
//
//  Created by Виктор on 25/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

final class PeopleTableViewCell: UITableViewCell {

    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
    }
    
    func bind(with userViewModel: UserViewModel) {
        fullNameLabel.text = userViewModel.fullName
        avatarImageView.image = UIImage(named: userViewModel.avatarUrl)
        distanceLabel.text = "\(userViewModel.distance) m"
    }
}
