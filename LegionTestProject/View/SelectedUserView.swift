//
//  SelectedUserView.swift
//  LegionTestProject
//
//  Created by Виктор on 30/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

final class SelectedUserView: UIView {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var coordinatesLabel: UILabel!
    
    var fullName: String = "" {
        didSet {
            nameLabel.text = fullName
        }
    }
    
    var position: Point = (x: 0, y: 0) {
        didSet {
            coordinatesLabel.text = "x:\(position.x) y:\(position.y)"
        }
    }
    
    var imageUrl: String = "" {
        didSet {
            avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
            avatarImageView.image = UIImage(named: imageUrl)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    private func initView() {
        let nibView = UINib(nibName: "SelectedUserView", bundle: nil)
        nibView.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
}
