//
//  MainScreenModule.swift
//  LegionTestProject
//
//  Created by Виктор on 01/07/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import UIKit

final class MainScreenModule {
    static func Configure() -> UIViewController {
        let vc = PeopleViewController()
        let baseUser = User(id: 0, firstName: "Victor", lastName: "Volkov", avatarUrl: "avatar-1", position: (x: 0, y: 0))
        let presenter = PeoplePresenter(with: baseUser)
        vc.presenter = presenter
        presenter.view = vc
        let navVC = UINavigationController.init(rootViewController: vc)
        navVC.navigationBar.isTranslucent = false
        return navVC
    }
}
