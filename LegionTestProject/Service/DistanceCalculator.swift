//
//  DistanceCalculator.swift
//  LegionTestProject
//
//  Created by Виктор on 26/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import UIKit

final class DistanceCalculator {
    
    /**
     Вычисление длины отрезка (расстояние между пользователями)
     - parameters:
        - fPoint: первая точка
        - sPoint: вторая точка
     - returns:
    Длина отрезка
     */
    static func distanceBetween(fPoint: Point, sPoint: Point) -> Int {
        return Int(sqrtf(powf(Float(fPoint.x - sPoint.x), 2.0) + powf(Float(fPoint.y - sPoint.y), 2.0)))
    }
    
    /**
     Вычисление положения пользователя (грубая оценка)
     - parameters:
        - point: стартовые координаты пользователя
        - speed: радиус окружности
        - angle: угол, под которым пошел пользователь
     - returns:
     Новые координаты пользователя
     */
    static func move(from point: Point, toAngle angle: Int, withSpeed speed: Int) -> Point {
        let radians = Double(angle) *  Double.pi/180.0
        let radius = Double(speed)
        let xPos = round(Double(point.x) + radius * cos(radians))
        let yPos = round(Double(point.y) + radius * sin(radians))
        return (x: Int(xPos), y: Int(yPos))
    }
}
