//
//  MockDataService.swift
//  LegionTestProject
//
//  Created by Виктор on 25/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

protocol ServerAPI {
    func getUsers(result: @escaping (Result<[User], TimeoutError>) -> ())
}

/**
 Подозреваю, что приложение должно обрабатывать кейс работы с сервером, но сервера нет, поэтому замокаем его и создадим класс-сервер, который хранит людей.
 Допустим, на сервере каждую секунду будут изменяться координаты пользователей и предположим, что пользователи будут "в поле", это значит, что они могут двигаться в любую сторону на 360 градусов.
 **/
final class MockServer {
    private static let firstNames = ["John", "Andrew", "Phil", "Alice", "Bob", "Matt", "Marylin", "Steven", "Franky", "Tim"]
    private static let lastNames = ["Cook", "Gerrard", "Manson", "Galloway", "Hamma", "Helm", "Cavana", "Holmes", "Duplantier"]
    
    // ЕИ: м/с
    static let shared = MockServer(totalUsers: 100, averageSpeed: 16)
    
    private var users = [User]()
    private var averageSpeed: Int
    private var timer: Timer?
    
    private init(totalUsers: Int, averageSpeed: Int) {
        self.averageSpeed = averageSpeed
        for i in 0..<totalUsers {
            let firstName = MockServer.firstNames.randomElement()!
            let lastName = MockServer.lastNames.randomElement()!
            let avatarUrl = "avatar-\(Int.random(in: 1..<9))"
            let xPos = Int.random(in: -10..<10)
            let yPos = Int.random(in: -10..<10)
            let user = User(id: i + 1, firstName: firstName, lastName: lastName, avatarUrl: avatarUrl, position: (x: xPos, y: yPos))
            users.append(user)
        }
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [unowned self] (t) in
            self.users = self.users.map { (user) -> User in
                var newUser = user
                newUser.position = DistanceCalculator.move(from: user.position, toAngle: Int.random(in: 0..<361), withSpeed: averageSpeed)
                return newUser
            }
        })
        timer?.tolerance = 0.3
        RunLoop.current.add(timer!, forMode: RunLoop.Mode.common)
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
}

extension MockServer: ServerAPI {
    func getUsers(result: @escaping (Result<[User], TimeoutError>) -> ()) {
        let maxVal = 1.0
        let timeout = Double.random(in: 0..<maxVal)
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) { [unowned self] in
            if timeout < maxVal * 0.75 {
                result(.success(self.users))
            } else {
                result(.failure(TimeoutError.timeExceeded))
            }
        }
    }
}
