//
//  TimeoutError.swift
//  LegionTestProject
//
//  Created by Виктор on 01/07/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

enum TimeoutError: Error {
    case timeExceeded
}

extension TimeoutError: LocalizedError {
    var errorDescription: String? {
        return "Превышен лимит ожидания"
    }
}
