//
//  PeopleViewController.swift
//  LegionTestProject
//
//  Created by Виктор on 25/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

protocol PeopleViewControllerOutput: class {
    func viewDidLoad()
    func didSelectUser(at index: Int)
    func didDeselectUser()
    func didChangeBaseUserCoordinates(xCoord: Int, yCoord: Int)
    func numberOfItems() -> Int
    func cell(at index: Int) -> UserViewModel
}

final class PeopleViewController: UIViewController {
    
    private static let cellID = "PeopleCellID"
    private static let cellName = "PeopleTableViewCell"
    
    @IBOutlet private weak var peopleTableView: UITableView!
    @IBOutlet private weak var xCoordField: UITextField!
    @IBOutlet private weak var yCoordField: UITextField!
    @IBOutlet private weak var acceptButton: UIButton!
    @IBOutlet private weak var selectedUserView: SelectedUserView!
    
    private lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.color = .black
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    enum ViewState: Int {
        case none, selected
    }
    
    private var state: ViewState = .none {
        didSet {
            let isEnabled = state == .none
            xCoordField.isEnabled = isEnabled
            yCoordField.isEnabled = isEnabled
            acceptButton.isEnabled = isEnabled
            UIView.animate(withDuration: 0.2) { [weak self] in
                self?.selectedUserView.isHidden = isEnabled
            }
            if isEnabled {
                presenter?.didDeselectUser()
            }
        }
    }
    
    var presenter: PeopleViewControllerOutput?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Пользователи"
        configureTableView()
        presenter?.viewDidLoad()
        selectedUserView.isHidden = true
        peopleTableView.rowHeight = 82
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideCardView))
        selectedUserView.addGestureRecognizer(tapGesture)
    }
    
    private func configureTableView() {
        peopleTableView.dataSource = self
        peopleTableView.delegate = self
        peopleTableView.register(UINib.init(nibName: PeopleViewController.cellName, bundle: nil), forCellReuseIdentifier: PeopleViewController.cellID)
        peopleTableView.keyboardDismissMode = .onDrag
        peopleTableView.isHidden = true
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    override func viewWillLayoutSubviews() {
        indicator.frame = view.bounds
    }
    
    @objc private func hideCardView() {
        state = .none
    }
    
    @IBAction private func changeCoordinatesButton(_ sender: Any) {
        let x = Int(xCoordField.text ?? "")
        let y = Int(yCoordField.text ?? "")
        view.endEditing(true)
        if let x = x, let y = y {
            presenter?.didChangeBaseUserCoordinates(xCoord: x, yCoord: y)
        } else {
            // Несмотря на number pad, пустой лейбл и копировать-вставить никто не отменял
            let alert = UIAlertController(title: "Ошибка", message: "Неверно введены координаты", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "Обидно", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
}

extension PeopleViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PeopleViewController.cellID) as! PeopleTableViewCell
        if let viewModel = presenter?.cell(at: indexPath.row) {
            cell.bind(with: viewModel)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfItems() ?? 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
}

extension PeopleViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        state = .selected
        presenter?.didSelectUser(at: indexPath.row)
    }
}

extension PeopleViewController: PeoplePresenterOutput {
    
    func updateTableView() {
        if peopleTableView.isHidden {
            peopleTableView.isHidden = false
            indicator.stopAnimating()
            indicator.removeFromSuperview()
        }
        peopleTableView.reloadData()
    }
    
    func moveTableViewCells(with points: [Point]) {
        guard points.count > 0 else { return }
        peopleTableView.beginUpdates()
        points.forEach { (start, end) in
            peopleTableView.moveRow(at: IndexPath(row: start, section: 0), to: IndexPath(row: end, section: 0))
        }
        peopleTableView.endUpdates()
    }
    
    func updateUserCard(fullName: String, position: Point, imageName: String) {
        selectedUserView.fullName = fullName
        selectedUserView.position = position
        selectedUserView.imageUrl = imageName
    }
}
