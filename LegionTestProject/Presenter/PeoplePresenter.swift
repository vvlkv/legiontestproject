//
//  PeoplePresenter.swift
//  LegionTestProject
//
//  Created by Виктор on 26/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

protocol PeoplePresenterOutput: class {
    func updateTableView()
    func moveTableViewCells(with points: [Point])
    func updateUserCard(fullName: String, position: Point, imageName: String)
}

final class PeoplePresenter {
    
    private var timer: Timer!
    private var baseUser: User
    private var selectedUser: User
    private var users = [User]()
    // Решил хранить VM тут, чтобы не усложнять ViewController
    private var usersViewModel = [UserViewModel]()
    
    weak var view: PeoplePresenterOutput?
    
    init(with baseUser: User) {
        self.baseUser = baseUser
        self.selectedUser = baseUser
    }
    
    @objc func obtainData() {
        MockServer.shared.getUsers { [weak self] (result) in
            switch result {
            case .success(let users):
                self?.users = users
                self?.updateUsersOnView(updateCard: true)
                break
            case .failure(let fail):
                print(fail.localizedDescription)
                break
            }
        }
    }
    
    private func updateUsersOnView(updateCard: Bool) {
        if usersViewModel.count == 0 {
            createUsersViewModel(for: selectedUser)
        } else {
            updateUsersViewModel(for: selectedUser)
        }
        self.view?.updateTableView()
        let pointsToMove = sortViewModel()
        self.view?.moveTableViewCells(with: pointsToMove)
        if updateCard {
            self.view?.updateUserCard(fullName: "\(selectedUser.firstName) \(selectedUser.lastName)", position: selectedUser.position, imageName: selectedUser.avatarUrl)
        }
    }
    
    /**
     При смене "Опорного" пользователя необходимо создать массив VM (изначально отсортирован)
     */
    private func createUsersViewModel(for dependenceUser: User) {
        let filteredUsers = users.filter { $0.id != dependenceUser.id }
        usersViewModel = filteredUsers.map({ (user) -> UserViewModel in
            return UserViewModel(
                id: user.id,
                fullName: "\(user.firstName) \(user.lastName)",
                avatarUrl: user.avatarUrl,
                distance: DistanceCalculator.distanceBetween(fPoint: dependenceUser.position, sPoint: user.position))
        }).sorted(by: { $0.distance < $1.distance})
    }
    
    /**
     При обновлении данных нужно пересчитать расстояние до пользователя, сохранив порядок VM, чтобы при обновлении tableView не было лишних перестановок
     */
    private func updateUsersViewModel(for dependenceUser: User) {
        let userId = selectedUser.id
        if let selectedUser = users.first(where: { $0.id == userId }) {
            self.selectedUser = selectedUser
        }
        usersViewModel = usersViewModel.map({ [weak self] (u) -> UserViewModel in
            guard let userModel = self?.users.first(where: { $0.id == u.id }) else { return u }
            var newUserViewModel = u
            newUserViewModel.distance = DistanceCalculator.distanceBetween(fPoint: dependenceUser.position, sPoint: userModel.position)
            return newUserViewModel
        })
    }
    
    /**
     Сортируем VM в зависимости от расстояния до пользователя, попутно возвращая массив поинтов, откуда-куда необходимо переместить ячейки нашей таблицы
     */
    private func sortViewModel() -> [Point] {
        var points = [Point]()
        let unsortedViewModels = usersViewModel
        usersViewModel = unsortedViewModels.sorted(by: { $0.distance < $1.distance })
        for (i, userViewModel) in unsortedViewModels.enumerated() {
            let toIndex = usersViewModel.firstIndex(where: { $0.id == userViewModel.id }) ?? 0
            if i != toIndex {
                points.append((x: i, y: toIndex))
            }
        }
        return points
    }
    
    deinit {
        timer.invalidate()
        timer = nil
    }
}

extension PeoplePresenter: PeopleViewControllerOutput {
    
    func viewDidLoad() {
        obtainData()
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(obtainData), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: .common)
    }
    
    func didSelectUser(at index: Int) {
        selectedUser = users.first(where: { $0.id == usersViewModel[index].id })!
        usersViewModel.removeAll()
        updateUsersOnView(updateCard: true)
    }
    
    func didDeselectUser() {
        selectedUser = baseUser
        usersViewModel.removeAll()
        updateUsersOnView(updateCard: false)
    }
    
    func didChangeBaseUserCoordinates(xCoord: Int, yCoord: Int) {
        let newPos = (x: xCoord, y: yCoord)
        baseUser.position = newPos
        // Тоже косяк. Изначально для юзера использовал класс вместо структуры, поэтому достаточно было поменять координаты у базового пользователя, ибо был ref type
        selectedUser.position = newPos
        updateUsersOnView(updateCard: false)
    }
    
    func numberOfItems() -> Int {
        return usersViewModel.count
    }
    
    func cell(at index: Int) -> UserViewModel {
        return usersViewModel[index]
    }
}
