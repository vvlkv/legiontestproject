//
//  LegionTestProjectTests.swift
//  LegionTestProjectTests
//
//  Created by Виктор on 26/06/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import XCTest
@testable import LegionTestProject

class LegionTestProjectTests: XCTestCase {
    
    func testDistance() {
        let err = "wrong distance"
        XCTAssert(DistanceCalculator.distanceBetween(fPoint: (0, 0), sPoint: (0, 30)) == 30, err)
        XCTAssert(DistanceCalculator.distanceBetween(fPoint: (0, 0), sPoint: (-20, 0)) == 20, err)
        XCTAssert(DistanceCalculator.distanceBetween(fPoint: (0, 0), sPoint: (0, -20)) == 20, err)
        XCTAssert(DistanceCalculator.distanceBetween(fPoint: (40, 23), sPoint: (34, 21)) == 6, err)
    }
    
    func testMove() {
        let speed = 4
        let point = (x:0, y: 0)
        let err = "wrong move"
        XCTAssert(DistanceCalculator.move(from: point, toAngle: 0, withSpeed: speed) == (x: 4, y:0), err)
        XCTAssert(DistanceCalculator.move(from: point, toAngle: 90, withSpeed: speed) == (x: 0, y:4), err)
        XCTAssert(DistanceCalculator.move(from: point, toAngle: 180, withSpeed: speed) == (x: -4, y:0), err)
        XCTAssert(DistanceCalculator.move(from: point, toAngle: 270, withSpeed: speed) == (x: 0, y:-4), err)
        XCTAssert(DistanceCalculator.move(from: point, toAngle: 180 + 45, withSpeed: speed) == (x: -3, y:-3), err)
    }
}
